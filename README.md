# Log4Shell sample application

This sample application shows how the [Log4Shell vulnerability](https://github.com/advisories/GHSA-jfh8-c2jp-5v3q) works.
It logs the message '${jndi:objects:evil}' using the application's logging framework.

It tries to cover most real life configuration with different maven profiles.
Activate the corresponding maven profile to see the effects.
Make sure to refresh the maven project and rebuild the project if profile was switched!

To see the fix working change log4j2.version in pom.xml from 2.14.1 to 2.15.0.

## spring-boot-default
This is a default spring boot logging config using slf4j with logback. 
A JNDI provider is NOT available and configured.

Expected log message: `${jndi:objects:evil}`

> No untrusted code is executed. No risk.

## log4j-plain
This is a plain log4j configuration. A JNDI provider is NOT available and configured.

Expected log message: `${jndi:objects:evil}`

Additionally an exception will be thrown as no JNDI provider is configured.

> No untrusted code is executed. No risk.

## log4j-to-slf4j-with-jndi
This is a configuration using log4j delegating to slf4j with logback.
A JNDI provider is available and configured.

Expected log message: `${jndi:objects:evil}`

> No untrusted code is executed. No risk.

## log4j-plain-jndi-not-configured
This is a plain log4j configuration.
A JNDI provider is available but NOT configured.

Expected log message: `${jndi:objects:evil}`

> No untrusted code is executed. No risk.

## log4j-over-slf4j-with-jndi
This is a configuration using log4j delegating to slf4j with simple implementation.
A JNDI provider is available and configured.

Expected log message: `${jndi:objects:evil}`

> No untrusted code is executed. No risk.

## log4j-plain-with-jndi
This is a plain log4j configuration.
A JNDI provider is available and configured.

Expected log message: &lt;Messages that the code produces&gt;

> Untrusted code is executed. Full risk.