package de.mthsoft.log4shell

import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application : CommandLineRunner {

    private val logger = LoggerFactory.getLogger(Application::class.java );

    override fun run(vararg args: String?) {
        test();
    }

    private fun test() {
        logger.info( "\${jndi:objects:evil}" );
    }

}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}